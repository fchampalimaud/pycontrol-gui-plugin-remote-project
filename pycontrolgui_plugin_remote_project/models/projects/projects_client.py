# !/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt4 import QtGui
from pysettings import conf
from pycontrolgui_plugin_remote_project.remote_models.project import Project
import rpyc

class ProjectsClient(object):

	def register_on_main_menu(self, mainmenu):
		super(ProjectsClient, self).register_on_main_menu(mainmenu)

		filemenu = mainmenu[0]
		#filemenu['File'].insert(8, '-')
		filemenu['File'].insert(3, {'Open a remote project': self.open_remote_project, 'icon': conf.REMOTEPROJECT_PLUGIN_ICON})

	def open_remote_project(self):
		text, ok = QtGui.QInputDialog.getText(self, 'Server info', 'Enter the server address and port:', text='localhost:18861')
		if ok:
			address, port = str(text).split(':')
			self.create_remote_project(address, int(port))

	def create_remote_project(self, remote_address, port):
		c = rpyc.connect(remote_address, port, keepalive=True)
		return Project(self, c)


	