# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolgui_plugin_remote_project.remote_models.base_client import BaseClient
from pycontrolgui.models.setup.board_task import BoardTask


class BoardTaskClient(BaseClient, BoardTask):
	

	def __init__(self, setup, remote_board_task):
		BoardTask.__init__(self, setup)
		BaseClient.__init__(self)

		self.remote_obj 		= remote_board_task

		self.variables 			= self.remote_obj.variables
		self.states 			= self.remote_obj.states
		self.events 			= self.remote_obj.events
		self.board 				= None
		self.task 				= None
		self.lowest_event_id 	= self.remote_obj.lowest_event_id

	"""
	@property
	def variables(self): 
		return self.remote_obj.variables if hasattr(self, 'remote_obj') else None
	@variables.setter
	def variables(self, value):
		BoardTask.variables.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.variables = value

	@property
	def states(self): 
		return self.remote_obj.states if hasattr(self, 'remote_obj') else None
	@states.setter
	def states(self, value):
		BoardTask.states.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.states = value

	@property
	def events(self): 
		return self.remote_obj.events if hasattr(self, 'remote_obj') else None
	@events.setter
	def events(self, value):
		BoardTask.events.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.events = value
	"""