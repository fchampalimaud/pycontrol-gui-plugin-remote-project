# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolgui_plugin_remote_project.remote_models.base_client import BaseClient
from pycontrolgui_plugin_remote_project.remote_models.session import Session
from pycontrolgui_plugin_remote_project.remote_models.setup.board_task import BoardTask
from pycontrolgui.models.setup import Setup

class SetupClient(BaseClient, Setup):
	

	def __init__(self, experiment, remote_setup):		
		Setup.__init__(self, experiment)
		BaseClient.__init__(self)

		self.remote_obj 	= remote_setup
		
		self.board_task 	= self.create_board_task(self.remote_obj.board_task)
		
		self.name 			= self.remote_obj.name
		self.path 			= self.remote_obj.path
		self.board 			= self.project.find_board(self.remote_obj.board.name) if self.remote_obj.board else None
		self.task 			= self.experiment.task

		for remote_session in self.remote_obj.sessions:  	self.create_session(remote_session)

		print(self.board)
		
	
	@property
	def path(self): 
		return self.remote_obj.path if hasattr(self, 'remote_obj') else None
	@path.setter
	def path(self, value):
		Setup.path.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.path = value
		
	
	@property
	def name(self): 
		return self.remote_obj.name if hasattr(self, 'remote_obj') else ''
	@name.setter
	def name(self, value):
		Setup.name.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.name = value


	def create_session(self, remote_session=None): 	
		if remote_session:
			return Session(self, remote_session)
		else:
			return Session(self, self.remote_obj.create_session() )
	

	def create_board_task(self, remote_board_task=None):
		if remote_board_task:
			return BoardTask(self, remote_board_task)
		else:
			if not hasattr(self, 'remote_obj'): return None
			return BoardTask(self, self.remote_obj.create_board_task() )