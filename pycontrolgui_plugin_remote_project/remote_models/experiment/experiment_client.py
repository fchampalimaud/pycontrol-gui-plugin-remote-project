# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolgui_plugin_remote_project.remote_models.base_client import BaseClient
from pycontrolgui.models.experiment import Experiment
from pycontrolgui_plugin_remote_project.remote_models.setup import Setup

class ExperimentClient(BaseClient, Experiment):
	

	def __init__(self, project, remote_exp):
		Experiment.__init__(self, project)
		BaseClient.__init__(self)

		self.remote_obj = remote_exp

		self.path 		= self.remote_obj.path
		self.name       = self.remote_obj.name
		self.task 		= self.project.find_task(self.remote_obj.task.name) if self.remote_obj.task else None

		for remote_setup in self.remote_obj.setups:  	self.create_setup(remote_setup)
		
		
	@property
	def path(self): 
		return self.remote_obj.path if hasattr(self, 'remote_obj') else None
	@path.setter
	def path(self, value):
		Experiment.path.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.path = value
		
	
	@property
	def name(self): 
		return self.remote_obj.name if hasattr(self, 'remote_obj') else ''
	@name.setter
	def name(self, value):
		Experiment.name.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.name = value


	

	def create_setup(self, remote_setup=None): 	
		if remote_setup:
			return Setup(self, remote_setup)
		else:
			return Setup(self, self.remote_obj.create_setup() )