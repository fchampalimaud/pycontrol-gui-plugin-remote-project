# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolgui_plugin_remote_project.remote_models.base_client import BaseClient
from pycontrolgui.models.session import Session

class SessionClient(BaseClient, Session):
	
	def __init__(self, setup, remote_session):
		Session.__init__(self, setup)
		BaseClient.__init__(self)

		self.remote_obj = remote_session


		self.name               = self.remote_obj.name
		self.path               = self.remote_obj.path
		self.setup_name			= self.remote_obj.setup_name
		self.board_name 		= self.remote_obj.board_name
		self.task_name 			= self.remote_obj.task_name
		self.board_serial_port  = self.remote_obj.board_serial_port
		self.started 			= self.remote_obj.started
		self.ended 				= self.remote_obj.ended
		

	@property
	def messages_history(self):
		#return rpyc.utils.classic.obtain(self.remote_obj.messages_history)

		return self.remote_obj.messages_history
	@messages_history.setter
	def messages_history(self, value): pass
	


	@property
	def path(self): 
		return self.remote_obj.path if hasattr(self, 'remote_obj') else None
	@path.setter
	def path(self, value):
		Session.path.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.path = value
		
	
	@property
	def name(self): 
		return self.remote_obj.name if hasattr(self, 'remote_obj') else ''
	@name.setter
	def name(self, value):
		Session.name.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.name = value
