# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolgui.models.project import Project

from pycontrolgui_plugin_remote_project.remote_models.experiment import Experiment
from pycontrolgui_plugin_remote_project.remote_models.task import Task
from pycontrolgui_plugin_remote_project.remote_models.board import Board


from pycontrolgui_plugin_remote_project.remote_models.base_client import BaseClient


class ProjectClient(Project,BaseClient):
	

	def __init__(self, projects, connection):
		Project.__init__(self, projects)
		BaseClient.__init__(self)

		self._connection = connection
		self.remote_obj  = connection.root.project

		

		self.name = self.remote_obj.name
		self.path = self.remote_obj.path

		for remote_board in self.remote_obj.boards:  	self.create_board(remote_board)
		for remote_task in self.remote_obj.tasks: 		self.create_task(remote_task)
		for remote_exp in self.remote_obj.experiments: 	self.create_experiment(remote_exp)
	
		
	
	@property
	def path(self): 
		return self.remote_obj.path if hasattr(self, 'remote_obj') else None
	@path.setter
	def path(self, value):
		Project.path.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.path = value
		
	@property
	def name(self): 
		return self.remote_obj.name if hasattr(self, 'remote_obj') else ''
	@name.setter
	def name(self, value):
		Project.name.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.name = value
	
	
	def create_experiment(self, remote_exp=None): 	
		if remote_exp:
			return Experiment(self, remote_exp)
		else:
			return Experiment(self, self.remote_obj.create_experiment() )

	def create_board(self, remote_board=None):
		if remote_board:
			return Board(self, remote_board)
		else:
			return Board(self, self.remote_obj.create_board() )

	def create_task(self, remote_task=None):
		if remote_task:
			return Task(self, remote_task)
		else:
			return Task(self, self.remote_obj.create_task() )