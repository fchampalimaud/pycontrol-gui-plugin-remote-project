# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolgui_plugin_remote_project.remote_models.base_client import BaseClient
from pycontrolgui.models.board import Board

class BoardClient(BaseClient, Board):
	

	def __init__(self, project, remote_board):
		Board.__init__(self, project)
		BaseClient.__init__(self)

		self.remote_obj = remote_board

		self.name        = self.remote_obj.name
		self.serial_port = self.remote_obj.serial_port

	@property
	def log_messages(self):
		return self.remote_obj.log_messages

	@log_messages.setter
	def log_messages(self, value): pass
	 

	
	@property
	def serial_port(self): 
		return self.remote_obj.serial_port if hasattr(self, 'remote_obj') else None
	@serial_port.setter
	def serial_port(self, value):
		Board.serial_port.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.serial_port = value
		
	@property
	def name(self): 
		return self.remote_obj.name if hasattr(self, 'remote_obj') else ''
	@name.setter
	def name(self, value):
		Board.name.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.name = value


	def create_install_framework_job(self, framework_path):
		return self.remote_obj.create_install_framework_job(framework_path)

	def create_job_upload_task(self, board_task):
		return self.remote_obj.create_job_upload_task(board_task.remote_obj)

	def create_job_run_task(self, session, board_task):
		return self.remote_obj.create_job_run_task(session.remote_obj, board_task.remote_obj)

	def create_job_sync_variables(self, board_task):
		return self.remote_obj.create_job_sync_variables(board_task.remote_obj)