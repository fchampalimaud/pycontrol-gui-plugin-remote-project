# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolgui_plugin_remote_project.remote_models.base_client import BaseClient
from pycontrolgui.models.task import Task

class TaskClient(BaseClient, Task):
	

	def __init__(self, project, remote_task):
		Task.__init__(self, project)
		BaseClient.__init__(self)

		self.remote_obj = remote_task

		self.name       = self.remote_obj.name
		self.path 		= self.remote_obj.path
		

	
	@property
	def path(self): 
		return self.remote_obj.path if hasattr(self, 'remote_obj') else None
	@path.setter
	def path(self, value):
		Task.path.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.path = value
		
	
	@property
	def name(self): 
		return self.remote_obj.name if hasattr(self, 'remote_obj') else ''
	@name.setter
	def name(self, value):
		Task.name.fset(self, value)
		if hasattr(self, 'remote_obj'): self.remote_obj.name = value